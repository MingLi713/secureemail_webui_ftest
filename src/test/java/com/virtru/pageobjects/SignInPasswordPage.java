package com.virtru.pageobjects;

import com.virtru.util.WebUtil;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created by mingli on 2018-01-27.
 */
public class SignInPasswordPage {

    public void fillInPassword(WebDriver driver, WebDriverWait wait) {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("span[class='RveJvd snByac']")));
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("input[class=\"whsOnd zHQkBf\"][type=\"password\"]")));
        WebElement passwordTextbox = driver.findElement(By.cssSelector("input[class=\"whsOnd zHQkBf\"][type=\"password\"]"));
        passwordTextbox.clear();
        passwordTextbox.sendKeys("virtru111");
    }

    public void clickNextButton(WebDriver driver, WebDriverWait wait) {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("span[class='RveJvd snByac']")));
//        WebElement clickButtonInPasswordPage = driver.findElement(By.cssSelector("span[class='RveJvd snByac']]"));
        WebElement clickButtonInPasswordPage = driver.findElement(By.cssSelector("span[class='RveJvd snByac']"));
        clickButtonInPasswordPage.click();
    }

    public static EmailMainPage gotoEmailMainPage(WebDriver driver) {
        return PageFactory.initElements(driver,EmailMainPage.class);
    }
}
