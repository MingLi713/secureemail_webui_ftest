package com.virtru.pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.Set;

/**
 * Created by mingli on 2018-01-27.
 */
public class EmailMainPage {

    public void checkInbox(WebDriver driver, WebDriverWait wait) {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.partialLinkText("Inbox")));
    }

    public void selectEncryptedEmail(WebDriver driver, WebDriverWait wait) {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("div[class='y6'] span[id] b")));
        WebElement clickEncryptedEmail = driver.findElement(By.cssSelector("div[class='y6'] span[id] b"));
        clickEncryptedEmail.click();
    }

    public void clickUnlockMessageButton(WebDriver driver, WebDriverWait wait) throws InterruptedException {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.partialLinkText("Unlock Message")));
        WebElement unlockMessageButton = driver.findElement(By.partialLinkText("Unlock Message"));

        unlockMessageButton.click();
    }

    public static SecureReaderPage gotoSecureReaderPage(WebDriver driver)  {
        return PageFactory.initElements(driver,SecureReaderPage.class);
    }

    public void signOutGmail(WebDriver driver, WebDriverWait wait) {
        // Get all window handles
        Set<String> allHandles = driver.getWindowHandles();

        // Get current handle to switch
        String currentWindowHandle = allHandles.iterator().next();
        driver.switchTo().window(currentWindowHandle);

        WebElement clickProfileButton = driver.findElement(By.cssSelector("span[class='gb_ab gbii']"));
        clickProfileButton.click();

        WebElement signOutLinkage = driver.findElement(By.id("gb_71"));
        signOutLinkage.click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("div[jsname=\"af8ijd\"]")));
        WebElement clickAccountList = driver.findElement(By.cssSelector("div[jsname=\"af8ijd\"]"));
        clickAccountList.click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("button[class = \"q4UYxb\"]")));
        WebElement removeButton = driver.findElement(By.cssSelector("button[class = \"q4UYxb\"]"));
        removeButton.click();

        WebElement signOutButton = driver.findElement(By.cssSelector("p[class=\"KlxXxd\"]"));
        signOutButton.click();

        wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("div[data-id = \"mHcrMb\"]")));
        WebElement yesRemoveButton = driver.findElement(By.cssSelector("div[data-id = \"mHcrMb\"]"));
        yesRemoveButton.click();
    }
}
