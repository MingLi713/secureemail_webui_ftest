package com.virtru.pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created by mingli on 2018-01-27.
 */
public class SecureReaderPage {

    public void selectEmailBox(WebDriver driver, WebDriverWait wait) {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("span[class='userEmail']")));
        WebElement selectOpenMethodButton = driver.findElement(By.cssSelector("span[class='userEmail']"));
        selectOpenMethodButton.click();
    }

    public void selectIdentity(WebDriver driver, WebDriverWait wait) {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("span[class='oauth-provider-google']")));
        WebElement googleMethodButton = driver.findElement(By.cssSelector("span[class='oauth-provider-google']"));
        googleMethodButton.click();
    }

    public void signOutSecureReader(WebDriver driver, WebDriverWait wait) {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("span[class='active-user']")));
        WebElement encryptedEmailAccount = driver.findElement(By.cssSelector("span[class='active-user']"));
        encryptedEmailAccount.click();
        driver.close(); // close newly opened window when done with it
    }

    public String getSecureEmailBody(WebDriver driver, WebDriverWait wait) {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"tdf-body\"]/div/p")));
        WebElement encryptedEmailBody = driver.findElement(By.xpath("//*[@id=\"tdf-body\"]/div/p"));
        String actualEmailBody = encryptedEmailBody.getText().trim();
        return actualEmailBody;
    }
}
