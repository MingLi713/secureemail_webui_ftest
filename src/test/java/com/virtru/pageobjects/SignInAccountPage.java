package com.virtru.pageobjects;

import com.virtru.util.WebUtil;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created by mingli on 2018-01-27.
 */
public class SignInAccountPage {

    public void fillInAccountName(WebDriver driver, WebDriverWait wait, String s) {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("identifierId")));
        WebElement usernameTextbox = driver.findElement(By.id("identifierId"));
        usernameTextbox.clear();
        usernameTextbox.sendKeys(s);
    }

    public void clickNextButton(WebDriver driver) {
        WebElement clickButtonInAccountPage = driver.findElement(By.id("identifierNext"));
        clickButtonInAccountPage.click();

    }

    public static SignInPasswordPage gotoSignInPasswordPage(WebDriver driver) {
        return PageFactory.initElements(driver,SignInPasswordPage.class);
    }
}
