package com.virtru.util;

import com.virtru.pageobjects.SignInAccountPage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by mingli on 2018-01-27.
 */
public class WebUtil {

    public static SignInAccountPage goToSignInAccountPage(WebDriver driver) {
        driver.get("http://www.gmail.com");
        return PageFactory.initElements(driver,SignInAccountPage.class);
    }
}
