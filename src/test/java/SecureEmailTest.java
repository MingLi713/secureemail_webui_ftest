import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import com.virtru.pageobjects.EmailMainPage;
import com.virtru.pageobjects.SecureReaderPage;
import com.virtru.pageobjects.SignInAccountPage;
import com.virtru.pageobjects.SignInPasswordPage;
import com.virtru.util.WebUtil;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.*;

import java.io.File;

/**
 * Created by mingli on 2018-01-27.
 */
public class SecureEmailTest {

    WebDriver driver;

    ExtentReports extent;
    ExtentTest logger;

    @BeforeTest
    // Initiate BrowserDrivers
    public void setBrowserDriver(){
        String browserName = System.getenv("browser");

        if (browserName != null && browserName.equalsIgnoreCase("chrome")){
            System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")+"//src//test//resourses//drivers//chromedriver");
            ChromeOptions options = new ChromeOptions();
            options.addArguments("disable-infobars");
            options.addArguments("-incognito");
            options.addArguments("start-maximized");
            driver = new ChromeDriver(options);
            driver.manage().window().maximize();
        }else{
            driver = new FirefoxDriver();
        }
    }

    @BeforeMethod
    // Initiate Extent Report Template
    public void initiateExtentReport(){
        extent = new ExtentReports(System.getProperty("user.dir")+"//test-output//AutomationTestingExtenReportofVirtru.html",true);
        extent.addSystemInfo("Host Name", "Automation Testing Server")
                .addSystemInfo("Environment","Automation Functional Testing")
                .addSystemInfo("User Name", "Ming Li");

        extent.loadConfig(new File(System.getProperty("user.dir")+"//extent-config.xml"));
    }


    @Test
    public void encryptedEmailReceive() throws InterruptedException {

        WebDriverWait wait = new WebDriverWait(driver, 30);

        //1. Login Gmail Website
        SignInAccountPage signInAccountPage = WebUtil.goToSignInAccountPage(driver);

        //2. Fill in Email or phone as account name
        signInAccountPage.fillInAccountName(driver, wait, "autotestervirtru@gmail.com");

        //3. Click Next button
        signInAccountPage.clickNextButton(driver);

        //4. Fill in password
        SignInPasswordPage signInPasswordPage = SignInAccountPage.gotoSignInPasswordPage(driver);
        signInPasswordPage.fillInPassword(driver, wait);

        //5. Click Next button to sign in
        signInPasswordPage.clickNextButton(driver,wait);

        //6. Click the encrypted email name to open it
        EmailMainPage emailMainPage = SignInPasswordPage.gotoEmailMainPage(driver);
        emailMainPage.checkInbox(driver, wait);
        emailMainPage.selectEncryptedEmail(driver,wait);

        //7. Click Unlock Message button
        String parentHandle = driver.getWindowHandle(); // Get the current window handle
        emailMainPage.clickUnlockMessageButton(driver,wait);
        SecureReaderPage secureReaderPage = EmailMainPage.gotoSecureReaderPage(driver);

        for (String winHandle : driver.getWindowHandles()) {
            driver.switchTo().window(winHandle); // Switch focus of WebDriver to the next found window handle (that's your newly opened window)
        }

        //8. Select the corresponding email box to open the encrypted email
        secureReaderPage.selectEmailBox(driver,wait);

        //9. Select LOGIN WITH Google identity button to open the encrypted email
        secureReaderPage.selectIdentity(driver,wait);

        //10. Verify Email Body
        String expectedEmailBody = "As J.J. Abrams and company get underway shooting Star Wars: Episode VII, they’re still adding pieces to the case. One of those pieces could be you in a very unusual move for the highly secretive film, a walk-on role for a member of the general public has been created and will be raffled off to benefit the charity UNICEF.";
        String actualEmailBody = secureReaderPage.getSecureEmailBody(driver, wait);


        logger = extent.startTest("testPass");
        Assert.assertEquals(actualEmailBody,expectedEmailBody.trim());
        logger.log(LogStatus.PASS,"Both expected and actual email boday are same.");

        //11. Sign Out Secure Reader
        secureReaderPage.signOutSecureReader(driver,wait);

        //12. Sign out Gmail
        emailMainPage.signOutGmail(driver,wait);
    }

    @AfterMethod
    // Collect test result of each method to create HTML Extent Report
    public void getResult(ITestResult result){
        if(result.getStatus() == ITestResult.FAILURE){
            logger.log(LogStatus.FAIL, "Test Case Failed is "+result.getName());
            logger.log(LogStatus.FAIL, "Test Case Failed is "+result.getThrowable());
        }else if(result.getStatus() == ITestResult.SKIP){
            logger.log(LogStatus.SKIP, "Test Case Skipped is "+result.getName());
        }
        // End test
        // endTest(logger) : It ends the current test and prepares to create HTML report
        extent.endTest(logger);
    }

    @AfterTest
    public void endReport(){
        // Writing everything to document
        // flush() - to write or update test information to your report.
        extent.flush();
        // Call close() at the very end of your session to clear all resources.
        // If any of your test ended abruptly causing any side-affects (not all logs sent to ExtentReports, information missing), this method will ensure that the test is still appended to the report with a warning message.
        // You should call close() only once, at the very end (in @AfterSuite for example) as it closes the underlying stream.
        // Once this method is called, calling any Extent method will throw an error.
        // close() - To close all the operation
        extent.close();
    }
    @AfterTest
    public void tearDown(){
        driver.quit();
    }
}
